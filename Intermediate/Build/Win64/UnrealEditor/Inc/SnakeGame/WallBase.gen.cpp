// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/WallBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWallBase() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AWallBase::StaticRegisterNativesAWallBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AWallBase);
	UClass* Z_Construct_UClass_AWallBase_NoRegister()
	{
		return AWallBase::StaticClass();
	}
	struct Z_Construct_UClass_AWallBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWallBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WallBase.h" },
		{ "ModuleRelativePath", "WallBase.h" },
	};
#endif
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWallBase_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AWallBase, IInteractable), false },  // 3580355467
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWallBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWallBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AWallBase_Statics::ClassParams = {
		&AWallBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWallBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWallBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWallBase()
	{
		if (!Z_Registration_Info_UClass_AWallBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AWallBase.OuterSingleton, Z_Construct_UClass_AWallBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AWallBase.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<AWallBase>()
	{
		return AWallBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWallBase);
	struct Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_WallBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_WallBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AWallBase, AWallBase::StaticClass, TEXT("AWallBase"), &Z_Registration_Info_UClass_AWallBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AWallBase), 3086019844U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_WallBase_h_893865970(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_WallBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_WallBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
