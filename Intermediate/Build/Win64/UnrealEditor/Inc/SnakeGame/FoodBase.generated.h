// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_FoodBase_generated_h
#error "FoodBase.generated.h already included, missing '#pragma once' in FoodBase.h"
#endif
#define SNAKEGAME_FoodBase_generated_h

#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_SPARSE_DATA
#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_RPC_WRAPPERS
#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFoodBase(); \
	friend struct Z_Construct_UClass_AFoodBase_Statics; \
public: \
	DECLARE_CLASS(AFoodBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AFoodBase) \
	virtual UObject* _getUObject() const override { return const_cast<AFoodBase*>(this); }


#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAFoodBase(); \
	friend struct Z_Construct_UClass_AFoodBase_Statics; \
public: \
	DECLARE_CLASS(AFoodBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AFoodBase) \
	virtual UObject* _getUObject() const override { return const_cast<AFoodBase*>(this); }


#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoodBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFoodBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodBase(AFoodBase&&); \
	NO_API AFoodBase(const AFoodBase&); \
public:


#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodBase(AFoodBase&&); \
	NO_API AFoodBase(const AFoodBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFoodBase)


#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_12_PROLOG
#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_RPC_WRAPPERS \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_INCLASS \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_SPARSE_DATA \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_INCLASS_NO_PURE_DECLS \
	FID_SnakeGame_Source_SnakeGame_FoodBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AFoodBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_SnakeGame_Source_SnakeGame_FoodBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
