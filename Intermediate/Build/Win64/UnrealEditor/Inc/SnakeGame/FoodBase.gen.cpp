// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/FoodBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFoodBase() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AFoodBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFoodBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AFoodBase::StaticRegisterNativesAFoodBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AFoodBase);
	UClass* Z_Construct_UClass_AFoodBase_NoRegister()
	{
		return AFoodBase::StaticClass();
	}
	struct Z_Construct_UClass_AFoodBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_Locations_Inner;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Locations_MetaData[];
#endif
		static const UECodeGen_Private::FArrayPropertyParams NewProp_Locations;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FoodMeshComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FoodMeshComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FoodClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_FoodClass;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_NewFood_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_NewFood;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FoodLocationY_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_FoodLocationY;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FoodLocationX_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_FoodLocationX;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFoodBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FoodBase.h" },
		{ "ModuleRelativePath", "FoodBase.h" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_AFoodBase_Statics::NewProp_Locations_Inner = { "Locations", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBase_Statics::NewProp_Locations_MetaData[] = {
		{ "ModuleRelativePath", "FoodBase.h" },
	};
#endif
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AFoodBase_Statics::NewProp_Locations = { "Locations", nullptr, (EPropertyFlags)0x0010000000000000, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodBase, Locations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AFoodBase_Statics::NewProp_Locations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::NewProp_Locations_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodMeshComponent_MetaData[] = {
		{ "Category", "FoodBase" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "FoodBase.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodMeshComponent = { "FoodMeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodBase, FoodMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodMeshComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodClass_MetaData[] = {
		{ "Category", "FoodBase" },
		{ "ModuleRelativePath", "FoodBase.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodClass = { "FoodClass", nullptr, (EPropertyFlags)0x0014000000010001, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodBase, FoodClass), Z_Construct_UClass_AFoodBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodClass_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBase_Statics::NewProp_NewFood_MetaData[] = {
		{ "Category", "FoodBase" },
		{ "ModuleRelativePath", "FoodBase.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFoodBase_Statics::NewProp_NewFood = { "NewFood", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodBase, NewFood), Z_Construct_UClass_AFoodBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFoodBase_Statics::NewProp_NewFood_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::NewProp_NewFood_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationY_MetaData[] = {
		{ "Category", "FoodBase" },
		{ "ModuleRelativePath", "FoodBase.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationY = { "FoodLocationY", nullptr, (EPropertyFlags)0x0010000000010005, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodBase, FoodLocationY), METADATA_PARAMS(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationY_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationX_MetaData[] = {
		{ "Category", "FoodBase" },
		{ "ModuleRelativePath", "FoodBase.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationX = { "FoodLocationX", nullptr, (EPropertyFlags)0x0010000000010005, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodBase, FoodLocationX), METADATA_PARAMS(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationX_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFoodBase_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodBase_Statics::NewProp_Locations_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodBase_Statics::NewProp_Locations,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodMeshComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodClass,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodBase_Statics::NewProp_NewFood,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationY,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodBase_Statics::NewProp_FoodLocationX,
	};
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AFoodBase_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AFoodBase, IInteractable), false },  // 3580355467
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFoodBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFoodBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AFoodBase_Statics::ClassParams = {
		&AFoodBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AFoodBase_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFoodBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFoodBase()
	{
		if (!Z_Registration_Info_UClass_AFoodBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AFoodBase.OuterSingleton, Z_Construct_UClass_AFoodBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AFoodBase.OuterSingleton;
	}
	template<> SNAKEGAME_API UClass* StaticClass<AFoodBase>()
	{
		return AFoodBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFoodBase);
	struct Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_FoodBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_FoodBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AFoodBase, AFoodBase::StaticClass, TEXT("AFoodBase"), &Z_Registration_Info_UClass_AFoodBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AFoodBase), 2299100U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_FoodBase_h_2620178397(TEXT("/Script/SnakeGame"),
		Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_FoodBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_SnakeGame_Source_SnakeGame_FoodBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
