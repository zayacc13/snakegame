	// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodBase.h"
#include "SnakeBase.h"
#include "Components/StaticMeshComponent.h"
//#include "Z:\DefoltProgs\UE4\UE_5.0\Engine\Source\Runtime\Engine\Classes\Components\StaticMeshComponent.h"


// Sets default values
AFoodBase::AFoodBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FoodMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FoodMeshComponent"));
	FoodMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	FoodMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFoodBase::BeginPlay()
{
	const int FoodLocationX_ = FoodLocationX;
	const int FoodLocationY_ = FoodLocationY;
	Super::BeginPlay();
	for (int i = 0; i < FoodLocationX_; i++)
	{
		for (int j = 0; j < FoodLocationY_; j++)
		{
			FVector NewSpawn(-1320 + 120 * i, -1320 + 120 * j, 0);
			Locations.Add(NewSpawn);
		}
	}
}

// Called every frame
void AFoodBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodBase::SpawnNewFood()
{
	FVector NewFoodLocation(Locations[rand() % (FoodLocationY* FoodLocationX)]);
	FTransform NewFoodTransform(NewFoodLocation);
	NewFood = GetWorld()->SpawnActor<AFoodBase>(FoodClass,NewFoodTransform) ;
}

void AFoodBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
			SpawnNewFood();
			if (Snake->LastMovementSpeed > 0.1)
			{
				Snake->LastMovementSpeed -= 0.025;
			}
		}
	}
}

